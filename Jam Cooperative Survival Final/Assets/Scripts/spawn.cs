﻿using UnityEngine;
using System.Collections;

public class spawn : MonoBehaviour {

	public GameObject enemy;
	public float spawntime = 3f;
	public Transform[] spawnPoints;
	public float max; // total de enemigos
	public bool enemyspawn = false;

	public float enemies_alive = 0; //empieza con zero , restarle cuando mueran para que pueda seguir

	void Start () {

			InvokeRepeating ("Spawn", spawntime, spawntime);
			
	}

	private void Spawn(){


		if (enemies_alive < max) {

				int spawnPointIndex = Random.Range (0, spawnPoints.Length);

				Instantiate (enemy, spawnPoints [spawnPointIndex].position, spawnPoints [spawnPointIndex].rotation);
				enemies_alive++;
			                   
				enemyspawn = true;
			}

		
		}





	}



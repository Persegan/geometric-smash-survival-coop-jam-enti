﻿using UnityEngine;
using System.Collections;

public class enemy : MonoBehaviour {

	public Transform target;

	
	public float jumpHeight = 4;
	public float timeToJumpApex = .4f;
	float accelerationTimeAirborne = .2f;
	float accelerationTimeGrounded = .1f;
	public float moveSpeed = 1;
	float rotationSpeed = 10;

	public float maxDistance;

	float gravity;
	float jumpVelocity;
	Vector3 velocity;
	float velocityXSmoothing;
	
	Controller2D controller;

	private Transform myTransform;
	
	private Animator anim;

	void Awake(){

		myTransform = transform;
	}

	void Start() {

		GameObject go = GameObject.FindGameObjectWithTag ("Player");
		target = go.transform;
		controller = GetComponent<Controller2D> ();
		anim = GetComponent<Animator> ();
		gravity = -(2 * jumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		maxDistance = 0.3f;

	}
	
	void Update() {
		
		anim.SetFloat("speed", moveSpeed);
		anim.SetFloat ("speed", Mathf.Abs (Input.GetAxis ("Horizontal")));
		//transform.position.x += Input.GetAxis ("Horizontal") * moveSpeed * Time.deltaTime;
		
		if (controller.collisions.above || controller.collisions.below) {
			velocity.y = 0;
		}

		Vector2 input = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));

		/*
		if (Input.GetAxisRaw ("Horizontal") != 0) {
			transform.eulerAngles = (Input.GetAxisRaw ("Horizontal") < 0) ? Vector3.up * 180 : Vector3.zero;
		}
		*/
			
		float targetVelocityX = input.x * moveSpeed;
		//velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?accelerationTimeGrounded:accelerationTimeAirborne);
		velocity.y += gravity * Time.deltaTime;
		controller.Move (velocity * Time.deltaTime);

		//mover


		if (Vector3.Distance (target.position, transform.position) < 5) {
			
			anim.SetBool ("Run", true);
			anim.SetBool ("Iddle", false);
			//if (Vector3.Distance (Player.position, transform.position) <= Distance) {*/
			
			//transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.position - transform.position), Time.deltaTime * Smooth);
			//Look At the Player
			if(Vector3.Distance(target.position,transform.position) > maxDistance)
				transform.position = Vector3.MoveTowards (transform.position, target.position, moveSpeed * Time.deltaTime);
			
			//Move Towards the Player
			//Move Towards the Player
			//}
		}
		
		else {
			anim.SetBool ("Run", false);
			anim.SetBool ("Iddle", true);
		}

		//voltear
	



		transform.eulerAngles = ( target.position.x  <= transform.position.x ) ? Vector3.up * 180 : Vector3.zero;


	
	
	}
	
}
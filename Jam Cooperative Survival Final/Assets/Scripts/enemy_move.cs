﻿using UnityEngine;
using System.Collections;

public class enemy_move : MonoBehaviour {

	public GameObject player1;
	public GameObject player2;
	public float moveSpeed = 20;
	private float distancia1;
	private float distancia2;
	
	// Use this for initialization
	void Start () 
	{
		player1 = GameObject.Find ("Player_1");
		player2 = GameObject.Find ("Player_2");
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{

		distancia1 = Vector2.Distance (player1.transform.position, gameObject.transform.position);
		distancia2 = Vector2.Distance (player2.transform.position, gameObject.transform.position);

		if (distancia1 >= distancia2)
		{
			transform.position = Vector2.MoveTowards (transform.position, player2.transform.position, moveSpeed * Time.deltaTime);
		}
		else
		{
			transform.position = Vector2.MoveTowards (transform.position, player1.transform.position, moveSpeed * Time.deltaTime);
		}
	}
}
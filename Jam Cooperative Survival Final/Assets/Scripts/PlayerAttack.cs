﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAttack : MonoBehaviour {


	public List<GameObject> targetslist;

	//cooldown

	public float attackTimer;
	public float coolDown;


	// Use this for initialization
	void Start () {

		targetslist = new List<GameObject> ();


		attackTimer = 0; 
		coolDown = 0;

	
	}
	
	// Update is called once per frame
	void Update () {
		if (attackTimer > 0) {
			attackTimer -= Time.deltaTime;
		}

		if (attackTimer < 0)
			attackTimer = 0;


		if (Input.GetKeyUp(KeyCode.Z) ) {

			if( attackTimer == 0){

				foreach (GameObject PNJ1 in targetslist ){
					Attack (PNJ1.gameObject);
				}

				attackTimer = coolDown;
			}

		} 

	
		
		
	}

	public void Attack(GameObject target) {

			float distance = Vector3.Distance (target.transform.position, transform.position);

			Vector3 dir = (target.transform.position - transform.position).normalized;

			float direction1 = Vector3.Dot (dir, transform.forward);
			float direction2 = Vector3.Dot (dir, transform.up);
			Debug.Log (direction1);

			if (distance < 0.4 && direction1 > 0 /*&& direction2 > 0*/) {
				EnemyHealth eh = (EnemyHealth)target.GetComponent ("EnemyHealth");
				eh.AdjustCurrentHealth (-10);

			
			}

	}
}


﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Controller2D))]
public class Player : MonoBehaviour {
	
	public float jumpHeight = 4;
	public float timeToJumpApex = .4f;
	float accelerationTimeAirborne = .2f;
	float accelerationTimeGrounded = .1f;
	float moveSpeed = 3;
	
	float gravity;
	float jumpVelocity;
	Vector3 velocity;
	float velocityXSmoothing;

	Controller2D controller;

	private Animator anim;


	void Start() {
		controller = GetComponent<Controller2D> ();
		anim = GetComponent<Animator> ();
		gravity = -(2 * jumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		print ("Gravity: " + gravity + "  Jump Velocity: " + jumpVelocity);
	}
	
	void Update() {






		anim.SetFloat("speed", moveSpeed);
		anim.SetFloat ("speed", Mathf.Abs (Input.GetAxis ("Horizontal")));
		//transform.position.x += Input.GetAxis ("Horizontal") * moveSpeed * Time.deltaTime;

		if (controller.collisions.above || controller.collisions.below) {
			velocity.y = 0;
		}
		
		Vector2 input = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));
		
		if (Input.GetKeyDown (KeyCode.Space) && controller.collisions.below) {
			velocity.y = jumpVelocity;
			anim.SetBool ("jump", true);
		} else {
			anim.SetBool ("jump", false);
		}
		//TRANSFORM

		if (Input.GetKeyUp (KeyCode.P) && controller.collisions.below) {

			anim.SetInteger("ev",1);

		}

		if (Input.GetKeyDown (KeyCode.Z) && controller.collisions.below) {
			
			anim.SetBool ("attack", true);
			
		} else {
			anim.SetBool ("attack", false);
		}


		//invertir

		if (Input.GetAxisRaw ("Horizontal") != 0) {
			transform.eulerAngles = (Input.GetAxisRaw ("Horizontal") < 0) ? Vector3.up * 180 : Vector3.zero;
		}




		float targetVelocityX = input.x * moveSpeed;
		velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?accelerationTimeGrounded:accelerationTimeAirborne);
		velocity.y += gravity * Time.deltaTime;
		controller.Move (velocity * Time.deltaTime);
	}
}
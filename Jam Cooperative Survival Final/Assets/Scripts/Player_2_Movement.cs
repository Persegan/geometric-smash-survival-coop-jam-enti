﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Player_2_Movement : MonoBehaviour {

	public float MovementSpeed;
	public GameObject Player1;
	public  Sprite Player2_Red;	
	public Sprite Player2_Green;
	public Sprite Player2_Blue;
	public GameObject efecto_golpe;
	public AudioSource attack_sound;
	public AudioSource enemy_kill_sound;
	public GameObject event_manager;
	public AudioSource enemy_fail;
	public GameObject Score_Manager;
	public Slider healthbar;
	public int health;


	private float distance;
	private int color_state;
	private BoxCollider2D boxcollider;
	private bool movimiento_ataque = false;
	private bool damaged = false;

    private List<Collider2D> enemies_within_attack_range = new List<Collider2D>();

    // Use this for initialization
    void Start () 
	{
		boxcollider = GetComponent<BoxCollider2D>();
		health = 100;
		damaged = false;
	}
	

	void Update () 
	{
		boxcollider.size = new Vector2 (26f, 10);
		boxcollider.offset = new Vector2 (0f, -11.7f);
		distance = Vector2.Distance (Player1.transform.position, gameObject.transform.position);
		if (health <= 0)
		{
			Application.LoadLevel("Game_Over");
		}
		if (movimiento_ataque == false) 
		{
			if (Input.GetAxisRaw ("Player2_Horizontal") > 0) 
			{
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (MovementSpeed, 0);
				transform.eulerAngles = new Vector3 (0, 0, 90);
			
			} 
			else if (Input.GetAxisRaw ("Player2_Horizontal") < 0) 
			{
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-MovementSpeed, 0);
				transform.eulerAngles = new Vector3 (0, 0, 270);
			}
			if (Input.GetAxisRaw ("Player2_Vertical") > 0)
			{
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, MovementSpeed);
				transform.eulerAngles = new Vector3 (0, 0, 180);			
			} 
			else if (Input.GetAxisRaw ("Player2_Vertical") < 0) 
			{
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, -MovementSpeed);
				transform.eulerAngles = new Vector3 (0, 0, 0);
			
			}
			if (Input.GetAxisRaw ("Player2_Vertical") == 1 && Input.GetAxisRaw ("Player2_Horizontal") == 1)
			{
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (MovementSpeed, MovementSpeed);
			}
			if (Input.GetAxisRaw ("Player2_Vertical") == -1 && Input.GetAxisRaw ("Player2_Horizontal") == -1)
			{
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-MovementSpeed, -MovementSpeed);
			}
			if (Input.GetAxisRaw ("Player2_Vertical") == 1 && Input.GetAxisRaw ("Player2_Horizontal") == -1) 
			{
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-MovementSpeed, MovementSpeed);
			}
			if (Input.GetAxisRaw ("Player2_Vertical") == -1 && Input.GetAxisRaw ("Player2_Horizontal") == 1) 
			{
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (MovementSpeed, -MovementSpeed);
			}
			
			if (Input.GetAxisRaw ("Player2_Vertical") == 0 && Input.GetAxisRaw ("Player2_Horizontal") == 0) 
			{
				gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);
			}
			if (Input.GetButtonDown ("Player_2_Dash"))
			{
				StartCoroutine(Dash_Coroutine());
			}

		}
		if (Input.GetButtonDown ("Player2_Attack"))
		{
			StartCoroutine (Attack_Coroutine());
		}
		if (distance < 40 && distance > 0)	
		{
			color_state = 1;
			gameObject.GetComponent<Animator>().SetInteger("color_state", 1);
		}
		
		if (distance > 40 && distance < 80)
		{
			color_state = 2;
			gameObject.GetComponent<Animator>().SetInteger("color_state", 2);
		}
		if (distance > 80)
		{
			color_state = 3;
			gameObject.GetComponent<Animator>().SetInteger("color_state", 3);
		}
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag != "wall" && collider.gameObject.tag != "Player2" && collider.gameObject.tag != "Player1")
        {
            enemies_within_attack_range.Add(collider);
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag != "wall" && collider.gameObject.tag != "Player2" && collider.gameObject.tag != "Player1")
        {
            for (int i = 0; i < enemies_within_attack_range.Count; i++)
            {
                if (enemies_within_attack_range[i] == collider)
                {
                    enemies_within_attack_range.RemoveAt(i);
                    break;
                }
            }

        }
    }



    void OnCollisionStay2D(Collision2D collider)
	{
		if (collider.gameObject.tag == "Wall" || collider.gameObject.tag == "Player1")
		{

		}
		else
		{
			if (damaged == false)
			{
				StartCoroutine (Damaged_Coroutine(collider.collider));
			}
		}

	}

	IEnumerator Attack_Coroutine()
	{
        for (int i = 0; i < enemies_within_attack_range.Count; i++)
        {
            if (enemies_within_attack_range[i].gameObject.tag == "Enemy_P2_red")
            {
                if (color_state == 1)
                {
                    Score_Manager.GetComponent<score>().Score_number += 100;
                    event_manager.GetComponent<EventManager>().tiempo_restante_segundos += 3;
                    Instantiate(efecto_golpe, new Vector3(enemies_within_attack_range[i].gameObject.transform.position.x, enemies_within_attack_range[i].gameObject.transform.position.y, enemies_within_attack_range[i].gameObject.transform.position.z), enemies_within_attack_range[i].gameObject.transform.rotation);
                    Destroy(enemies_within_attack_range[i].gameObject);
                    //enemies_within_attack_range.RemoveAt(i);
                    enemy_kill_sound.Play();

                }
                else
                {
                    StartCoroutine(PushEnemy_Coroutine(enemies_within_attack_range[i]));
                }
            }
            else if (enemies_within_attack_range[i].gameObject.tag == "Enemy_P2_blue")
            {
                if (color_state == 3)
                {
                    Score_Manager.GetComponent<score>().Score_number += 100;
                    event_manager.GetComponent<EventManager>().tiempo_restante_segundos += 3;
                    Instantiate(efecto_golpe, new Vector3(enemies_within_attack_range[i].gameObject.transform.position.x, enemies_within_attack_range[i].gameObject.transform.position.y, enemies_within_attack_range[i].gameObject.transform.position.z), enemies_within_attack_range[i].gameObject.transform.rotation);
                    Destroy(enemies_within_attack_range[i].gameObject);
                    //enemies_within_attack_range.RemoveAt(i);
                    enemy_kill_sound.Play();
                }
                else
                {
                    StartCoroutine(PushEnemy_Coroutine(enemies_within_attack_range[i]));
                }
            }
            else if (enemies_within_attack_range[i].gameObject.tag == "Enemy_P2_green")
            {
                if (color_state == 2)
                {
                    Score_Manager.GetComponent<score>().Score_number += 100;
                    event_manager.GetComponent<EventManager>().tiempo_restante_segundos += 3;
                    Instantiate(efecto_golpe, new Vector3(enemies_within_attack_range[i].gameObject.transform.position.x, enemies_within_attack_range[i].gameObject.transform.position.y, enemies_within_attack_range[i].gameObject.transform.position.z), enemies_within_attack_range[i].gameObject.transform.rotation);
                    Destroy(enemies_within_attack_range[i].gameObject);
                    //enemies_within_attack_range.RemoveAt(i);
                    enemy_kill_sound.Play();
                }
                else
                {
                    StartCoroutine(PushEnemy_Coroutine(enemies_within_attack_range[i]));
                }
            }
            else if (enemies_within_attack_range[i].gameObject.tag == "Enemy_P2")
            {
                Score_Manager.GetComponent<score>().Score_number += 100;
                event_manager.GetComponent<EventManager>().tiempo_restante_segundos += 3;
                Instantiate(efecto_golpe, new Vector3(enemies_within_attack_range[i].gameObject.transform.position.x, enemies_within_attack_range[i].gameObject.transform.position.y, enemies_within_attack_range[i].gameObject.transform.position.z), enemies_within_attack_range[i].gameObject.transform.rotation);
                Destroy(enemies_within_attack_range[i].gameObject);
                //enemies_within_attack_range.RemoveAt(i);
                enemy_kill_sound.Play();
            }
            else if (enemies_within_attack_range[i].gameObject.tag == "Enemy_P1" || enemies_within_attack_range[i].gameObject.tag == "Enemy_P1_red" || enemies_within_attack_range[i].gameObject.tag == "Enemy_P1_green" || enemies_within_attack_range[i].gameObject.tag == "Enemy_P1_blue")
            {
                StartCoroutine(PushEnemy_Coroutine(enemies_within_attack_range[i]));
            }
        }

        gameObject.GetComponent<Animator> ().SetBool ("attack", true);
		attack_sound.Play();
		gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);
		movimiento_ataque = true;
		yield return new WaitForSeconds (0.15f);
		gameObject.GetComponent<Animator> ().SetBool ("attack", false);
		movimiento_ataque = false;	
		
	}
	IEnumerator Dash_Coroutine()
	{
		movimiento_ataque = true;
		GetComponent<Rigidbody2D> ().velocity = -gameObject.transform.up * 100;
		yield return new WaitForSeconds(0.25f);
		GetComponent<Rigidbody2D> ().velocity = -gameObject.transform.up * 0;
		movimiento_ataque = false;
	}
	IEnumerator PushEnemy_Coroutine(Collider2D collider)
	{
		collider.gameObject.GetComponent<Rigidbody2D> ().velocity = -transform.up * 325;
		yield return new WaitForSeconds(0.1f);
		collider.gameObject.GetComponent<Rigidbody2D> ().velocity = transform.up * 00;
	}

	IEnumerator Damaged_Coroutine(Collider2D collider)
	{
		enemy_fail.Play ();
		gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (gameObject.transform.position.x - collider.transform.position.x, gameObject.transform.position.y - collider.transform.position.y) * 8;  ;
		movimiento_ataque = true;
		health -= 10;
		healthbar.value -= 10;
		damaged = true;
		yield return new WaitForSeconds(0.1f);
		movimiento_ataque = false;
		yield return new WaitForSeconds(0.8f);
		damaged = false;
	}

}

﻿using UnityEngine;
using System.Collections;

public class Health2 : MonoBehaviour {

	
	public int maxhealth = 100;
	public int currentHealth = 100;
	
	public float HealthBarLength;
	
	// Use this for initialization
	void Start () {
		HealthBarLength = Screen.width / 2;
	}
	
	// Update is called once per frame
	void Update () {
		
		AdjustCurrentHealth (0);

		if (currentHealth == 0) {
			
			Application.LoadLevel("Game_Over");
			
		}
		
	}
	

	
	public void AdjustCurrentHealth(int adj){
		
		currentHealth += adj;
		
		if (currentHealth < 1) {
			currentHealth = 0;
		}
		
		if (currentHealth > maxhealth)
			currentHealth = maxhealth;
		
		if (maxhealth == 1)
			maxhealth = 1;
		
		
		HealthBarLength = (Screen.width / 2) * (currentHealth / (float)maxhealth);
		
	}
}

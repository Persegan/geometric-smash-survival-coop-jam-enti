﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class score_gameOver : MonoBehaviour {
	public GameObject Score;
	public Text text;

	// Use this for initialization
	void Start () 
	{
		Score = GameObject.Find ("Score_Manager");
	}
	
	// Update is called once per frame
	void Update () 
	{
		text.text = "Score:";
		text.text += Score.GetComponent<score> ().Score_number;
	
	}
}

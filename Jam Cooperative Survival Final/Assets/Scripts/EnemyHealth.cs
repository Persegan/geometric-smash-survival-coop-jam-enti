﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
	


	public int maxhealth = 50;
	public int currentHealth = 50;
	public bool killed = false;
	
	public float HealthBarLength;
	
	// Use this for initialization
	void Start () {
		HealthBarLength = Screen.width / 2;



	}
	
	// Update is called once per frame
	void Update () {

		
		AdjustCurrentHealth (0);
		dead ();
		
	}
	
	void OnGUI(){
		
		GUI.Box(new Rect(10,40,HealthBarLength,20),currentHealth+"/"+maxhealth);
	}
	
	public void AdjustCurrentHealth(int adj){
		
		currentHealth += adj;
		
		if (currentHealth < 1) {
			currentHealth = 0;
		}
		
		if (currentHealth > maxhealth)
			currentHealth = maxhealth;
		
		if (maxhealth == 1)
			maxhealth = 1;
		
		
		HealthBarLength = (Screen.width / 2) * (currentHealth / (float)maxhealth);
		
	}


	void dead(){

		if (currentHealth <= 0) {

			killed = true;
			
			Destroy(gameObject);
		}




	}
}

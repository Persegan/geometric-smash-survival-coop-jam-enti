using UnityEngine;
using System.Collections;

public class PNJ_Follow : MonoBehaviour {
	private Transform Player;
	public float Speed = 1;
	private float Distance = 15;
	bool attack;
	bool stay;
	bool isright;
	private Animator anim;
	private int x = 0;

	//private float Smooth = 2;

	// Use this for initialization
	void Start () {

		attack = false;
		stay = true;

		anim = this.GetComponent<Animator>();

		Player = GameObject.FindGameObjectWithTag("Player").transform;
		anim.SetBool ("Run", false);
		anim.SetBool ("Iddle", true);
		anim.SetBool ("Attack", false);
	}
	
	// Update is called once per frame
	void Update () {


		if (Player.position.x > transform.position.x ) {
			//player is to the right of enemy
			isright = true;

		}

		else{
			//player is the left
			isright = false;

		}/*
		if (Player.position.x > transform.position.x && isright != true) {

			Flip ();
			isright = false;
		} 

		if (isright != false &&  Player.position.x < transform.position.x){
			isright = true;
			Flip ();
		}*/


		if (Vector3.Distance (Player.position, transform.position) < 5) {

			anim.SetBool ("Run", true);
			anim.SetBool ("Iddle", false);
			stay = false;
			/*bb if (Vector3.Distance (Player.position, transform.position) <= Distance) {*/
				
				//transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Player.position - transform.position), Time.deltaTime * Smooth);
				//Look At the Player
				
				transform.position = Vector3.MoveTowards (transform.position, Player.position, Speed * Time.deltaTime);

			//Move Towards the Player
				//Move Towards the Player
			//}
		}

		else {
			anim.SetBool ("Run", false);
			anim.SetBool ("Iddle", true);
			stay = true;
			attack = false;
		}



		if (Vector3.Distance (Player.position, transform.position) < 1.3) {
			attack = true;
			stay = false;
			anim.SetBool ("Attack", true);

		}

		else {

			attack=false;
			anim.SetBool ("Attack", false);	

		}


		/*PREGUNTAR
		else{
			anim.SetBool ("Attack", false);
		}*/	
	}


	//Utilitzar Flip() perque el PNJ si el jugador esta a l'esquerra inverteixi l'sprite
	void Flip(){


		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	
}



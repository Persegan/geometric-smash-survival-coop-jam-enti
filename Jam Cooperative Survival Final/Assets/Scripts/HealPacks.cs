﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HealPacks : MonoBehaviour {



	public Transform player1;
	public Transform player2;
	
	float distance = 5f;




	// Use this for initialization
	void Start () {


		GameObject pj1 = GameObject.FindGameObjectWithTag ("Player1");
		player1 = pj1.transform;

		GameObject pj2 = GameObject.FindGameObjectWithTag ("Player2");
		player2 = pj2.transform;


	
	}
	
	// Update is called once per frame
	void Update () {

		Pack ();


	
	}

	private void Pack(){

		if (Vector3.Distance (player1.position, transform.position) <= distance) {
			
			Health eh = (Health)player1.GetComponent ("Health");
			eh.AdjustCurrentHealth (50);
			Destroy(gameObject);
			
			
		}
		
		if (Vector3.Distance (player2.position, transform.position) <= distance) {
			
			Health2 eh = (Health2)player2.GetComponent ("Health2");
			eh.AdjustCurrentHealth (50);
			Destroy(gameObject);
			
		}

	}
}

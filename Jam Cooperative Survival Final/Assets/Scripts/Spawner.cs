﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Spawner : MonoBehaviour {
		
		public float TiempoRespawn;
		public GameObject cosa1;
		public GameObject cosa2;
		public GameObject cosa3;
		public GameObject cosa4;
		public GameObject cosa5;
		public GameObject cosa6;
		public GameObject cosa7;
		public GameObject cosa8;
		public float MaxX;
		public float MaxY;
		public float MinX;
		public float MinY;
		public float tiempo;
		private float x;
		private float y;
		private int cosa_a_instanciar;
		private int oleada;
		private int oleada_requisito;
		
		
		
		// Use this for initialization
		void Start () 
		{
		oleada = 0;	
		oleada_requisito = 6;
		}
		
		// Update is called once per frame
		void FixedUpdate () 
		{
			tiempo -= Time.deltaTime; 
			if (tiempo < 0)
			{
				Instanciar();
				tiempo = TiempoRespawn;
				oleada +=1;

			}
			if (oleada == oleada_requisito)
			{
				oleada = 0;
				oleada_requisito += 2;+
				TiempoRespawn -= 1;

			}
			
			
			
		}
		void Instanciar()
		{
		cosa_a_instanciar = Random.Range (1, 8);
		if (cosa_a_instanciar == 1)
		{
			Instantiate (cosa1, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), new Quaternion());
		}

		else if (cosa_a_instanciar == 2)
		{
			Instantiate (cosa2, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), new Quaternion());
		}
		else if (cosa_a_instanciar == 3)
		{
			Instantiate (cosa3, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), new Quaternion());	
		}
		else if (cosa_a_instanciar == 4)
		{
			Instantiate (cosa4, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), new Quaternion());	
		}
		else if (cosa_a_instanciar == 5)
		{
			Instantiate (cosa5, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), new Quaternion());
		}
		else if (cosa_a_instanciar == 6)
		{
			Instantiate (cosa6, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), new Quaternion());	
		}
		else if (cosa_a_instanciar == 7)
		{
			Instantiate (cosa7, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), new Quaternion());
		}
		else if (cosa_a_instanciar == 8)
		{
			Instantiate (cosa8, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y), new Quaternion());
		}
	
			
			
		}
		
	}

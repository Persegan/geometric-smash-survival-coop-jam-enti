﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EventManager : MonoBehaviour {
	public Text Timer;
	public float tiempo_actualizar;
	public float tiempo_restante_segundos;
	public float tiempo_restante_minutos;
	// Use this for initialization
	void Start () 
	{
		tiempo_actualizar = 0;
		tiempo_restante_segundos = 65;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{

		tiempo_actualizar -= Time.deltaTime;
		if (tiempo_restante_segundos == 0 && tiempo_restante_minutos == 0)
		{	
			Application.LoadLevel("Game_Over");
		}
		else if (tiempo_restante_segundos == 0)
		{
			tiempo_restante_minutos -= 1;
			tiempo_restante_segundos = 60;
		}
		if (tiempo_restante_segundos >  60)
		{
			tiempo_restante_minutos +=1;
			tiempo_restante_segundos = tiempo_restante_segundos-60;
		}
		if (tiempo_actualizar < 0)
		{
			tiempo_restante_segundos -= 1;
			Timer.text = tiempo_restante_minutos.ToString();
			Timer.text += ":";
			if (tiempo_restante_segundos <10)
			{
				Timer.text += "0";
				Timer.text += tiempo_restante_segundos.ToString();
			}
			else
			{
				Timer.text += tiempo_restante_segundos.ToString();
			}
			tiempo_actualizar = 1;
		}
	}

}
